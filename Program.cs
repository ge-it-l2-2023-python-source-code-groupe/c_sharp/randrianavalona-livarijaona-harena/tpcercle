﻿using tpCercle;

class Program
{
    static void Main(string[] args)
    {
        //Instanciation d'un point, centre du cercle
        Point centre1 = new Point();
        Console.Write("Donner l'abcisse du centre : ");
        centre1.Abcisse  = Convert.ToDouble(Console.ReadLine());
        Console.Write("Donner l'ordonnée du centre : ");
        centre1.Ordonne  = Convert.ToDouble(Console.ReadLine());
        //Rayon du cerle
        Console.Write("Donner le rayon : ");
        double r = Convert.ToDouble(Console.ReadLine());
        //Instanciation du cercle avec comme centre le point centre et rayon r
        
        Cercle cercle1 = new Cercle(centre1, r);
        Console.WriteLine("");
        cercle1.Display();
        Console.WriteLine("");

        Console.WriteLine("Le perimètre est : "+cercle1.getPerimeter());
        Console.WriteLine("La surface est : "+cercle1.getSurface());
        Console.WriteLine("");

        Console.WriteLine("Donner un point");
        Console.Write("X : ");
        double x = Convert.ToDouble(Console.ReadLine());
        Console.Write("Y : ");
        double y = Convert.ToDouble(Console.ReadLine());
        Point newPoint = new Point(x, y);
        Console.WriteLine("");
        newPoint.Display();
        Console.WriteLine("");

        if (cercle1.Isinclude(newPoint))
        {
            Console.WriteLine("Le point appartient au cercle");
            
        }
        else
        {
            Console.WriteLine("Le point n'appartient pas au cercle");
        }
    }
}
