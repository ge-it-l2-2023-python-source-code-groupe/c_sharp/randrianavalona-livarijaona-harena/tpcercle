namespace tpCercle{
    public class Point
    {
        public double Abcisse {get; set;}
        public double Ordonne {get; set;}

        
        public Point(double x, double y){
            Abcisse = x;
            Ordonne = y;
        }
        public Point(){
            Abcisse = 0.0;
            Ordonne = 0.0;
        }
        public void Display(){
            Console.WriteLine("POINT("+Abcisse+", "+Ordonne+")");
        }
    }
    public class Cercle
    {
        public Point Center {get; set;}
        public double Rayon {get; set;}
        //this() miantso an'ilay constructeur Cercle() aloha izay vao mandeha ilay constructeur
        public Cercle(Point center, double rayon):this(){
            Center=center;
            Rayon=rayon;
        }
        public Cercle(){
            Center=new Point();
            Rayon=1.0;
        }
        public void Display(){
            Console.WriteLine($"CERCLE({Center.Abcisse}, {Center.Ordonne}, {Rayon})");
        }
        // perimetre = 2pi*r
        public string getPerimeter(){
            double p = 2*3.14*Rayon;
            string perim = p.ToString("0.00");
            return perim;
        }
        // surface = pi*r^2
        public string getSurface(){
            double s = 3.14*Rayon*Rayon;
            string surf = s.ToString("0.00");
            return surf; 
        }
        public bool Isinclude(Point p){
            // formule si Point est inclu dans un cercle : racine carre de {(xPoint-xCentre)^2 + (yPoint-yCentre)^2}
            if (Math.Sqrt(Math.Pow(p.Abcisse-Center.Abcisse,2)+Math.Pow(p.Ordonne-Center.Ordonne,2))<=Rayon)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}